import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Signup from '@/components/auth/Signup'
import Login from '@/components/auth/Login'
import Profile from '@/components/pageUser/Profile'
import MyJournal from '@/components/pagemyjournal/myjournal'
import AllMyJournals from '@/components/pagemyjournal/allmyjournals'
import FlashCard from '@/components/flashCard/FlashCard'
import FlashSound from '@/components/flashCard/FlashSound'
import testBoot from '@/components/layout/testBoot'
import WhatsMyJournal from '@/components/pageNoUser/WhatsMyJournal'
import Score from '@/components/flashCard/Score'
import Chart from '@/components/chart/Chart'
import Chart2 from '@/components/chart/Chart2'
import ChartRead from '@/components/chart/ChartRead'
import ChartWrite from '@/components/chart/ChartWrite'
import TextEditor from '@/components/editor/TextEditor'
import ChineseEditor from '@/components/editor/ChineseEditor'
import PinyinEditor from '@/components/editor/PinyinEditor'

import firebase from 'firebase'

Vue.use(Router)

const router =  new Router({
 
  routes: [
    {
      path: '/:id',
      name: 'Home',
      component: Home
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/profile/:id',
      name: 'Profile',
      component: Profile,
      meta:{
        requiresAuth : true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/myjournal/:id/:date',
      name: 'MyJournal',
      component: MyJournal
    },
    {
      path: '/allmyjournals/:id/',
      name: 'AllMyJournals',
      component: AllMyJournals
    },
    {
      path: '/flashcard/:id/:date',
      name: 'FlashCard',
      component: FlashCard
    },
    {
      path: '/flashsound/:id/:date',
      name: 'FlashSound',
      component: FlashSound
    },
    {
      path: '/testboot/',
      name: 'testBoot',
      component: testBoot
    },
    {
      path: '/whatsmyjournal/',
      name: 'WhatsMyJournal',
      component: WhatsMyJournal
    },
    {
      path: '/score/:id/',
      name: 'Score',
      component: Score
    },
    {
      path: '/chart/:id/',
      name: 'Chart',
      component: Chart
    },
    {
      path: '/chart2/:id/',
      name: 'Chart2',
      component: Chart2
    },
   
    {
      path: '/chartwrite/:id/',
      name: 'ChartWrite',
      component: ChartWrite
    },
    {
      path: '/chartread/:id/',
      name: 'ChartRead',
      component: ChartRead
    },
    {
      path: '/texteditor/:id/',
      name: 'TextEditor',
      component: TextEditor
    },
    {
      path: '/chineseeditor/:id',
      name: 'ChineseEditor',
      component: ChineseEditor
    },
    {
      path: '/pinyineditor/:id/',
      name: 'PinyinEditor',
      component: PinyinEditor
    }



  

  ]
})

router.beforeEach((to, from, next)=>{
// check if route require Auth
if (to.matched.some(rec => rec.meta.requiresAuth)){
// check auth state of user
let user = firebase.auth().currentUser
if(user){
  //user signed in, process to route
  next()
}else {
  // no user signd it, redirect to login
  next({name: 'Login'})
}

} else {
  next()
}
})

export default router
