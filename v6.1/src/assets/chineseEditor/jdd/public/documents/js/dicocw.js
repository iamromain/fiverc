function dicoCwShow(n, p) {
	for(i=1; i<=4; i++) {
		title = document.getElementById('t-'+n+'-'+i);
		part = document.getElementById('p-'+n+'-'+i);
		if(!title || !part) continue;
		titleClass = title.className.substring(0, title.className.length-1);
		if(i==p) {
			title.className = 'on';
			part.style.display = 'block';
		} else {
			title.className = 'off';
			part.style.display = 'none';
		}
	}
}