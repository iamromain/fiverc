/**
 *
 *	author	:	Florian Klingner alias castor
 * 	version	:	.1 , 2005-05-04
 *
 */





	/**
	 *	Creates a new instace of Timer. A timer can be started, stopped and paused.
	 *	It calls its 'action()' method every x milliseconds (x being the time specified by its
	 *	clock property).
	 * 	By default the method only writes some text to the status bar.
	 *
	 *	@param clock 	optional - sets the clock time in ms, defaults to 1000
	 */
	function Timer() {
		this.clock = 1000
		this.interval = null
		this.status = 0
		this.statusText = ["stopped", "started", "paused"]
		var This = this

		if (arguments.length > 0) {			
			if (!isNaN(arguments[0]))
				this.clock = arguments[0]
		}
		


		this.start = function() {
			if (This.interval != null) return

			This.action()
			this.interval = setInterval(function() { This.action() }, This.clock)
			This.status = 1
		}



		this.stop = function() {
			if (This.interval == null) return

			clearInterval(This.interval)
			This.interval = null
			This.status = 0
		}


	
		this.pause = function(dur) {
			if (This.interval == null) return

			This.stop()
			setTimeout(function() { This.start() }, dur)
			This.status = 2
		}



		this.action = function() {			
			window.status = "[Timer] default action"
		}



		this.getStatus = function() {
			return This.statusText[This.status]
		}
	}