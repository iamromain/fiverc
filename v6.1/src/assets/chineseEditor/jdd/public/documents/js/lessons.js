/**
 *
 *	author	:	Chine-Nouvelle.com
 * 	version	:	.1 , 2005-05-04
 *
 */


var lessonFile;
var lessonSpeed = 1;
var lessonNumber = 1;
var lessonPreviousLessons = 1;
var lessonStart = "Start";
var lessonStop = "Stop";

var t1Time = lessonSpeed * 1000;
var t2Time = t1Time * 4;

var ie=document.all
var ns6=document.getElementById && !document.all
var div;


var entries;
var nbEntries;
var currentEntryCursor = 0;
var cols = new Array("exoCn", "exoPinyin", "exoDef");
var nbCols = cols.length;
var currentCol = -1;
var currentEntry;

var t1 = new Timer(t1Time);
t1.action = function() {
	if(currentCol == -1) { resetEntry(); }
	else if(currentCol >= nbCols) { this.stop(); return; }
	else displayContents(currentEntry[currentCol], cols[currentCol]);
	currentCol++; }

var t2 = new Timer(t2Time);
t2.action = function() { setEntry(); }


function startStop() { if(t2.status==0) start(); else stop(); }

function start() {
	loadData();
	t2.start();
	document.getElementById('exoButton').value = lessonStop; }

function stop() {
	t1.stop();
	t2.stop();
	resetEntry();
	document.getElementById('exoButton').value = lessonStart; }

function getEntry() {
	if (nbEntries == 0) return false;
	if (currentEntryCursor >= nbEntries) currentEntryCursor = 0;
	if (currentEntryCursor == 0) sortEntries() 
	var entry = entries[currentEntryCursor];
	currentEntryCursor++;
	return entry; }

function setEntry() {
	resetEntry();
	currentEntry = getEntry();
	currentCol = -1;
	t1.start(); }

function resetEntry() { for(var i=0; i<currentCol; i++) displayContents("", cols[i]); }

function displayContents(contents, divId) {
	div = document.getElementById(divId);
	div.innerHTML = contents; }

function randOrd(a, b) { return (Math.round(Math.random())-0.5); }

function sortEntries() { entries.sort( randOrd ); }

function init() {
	stop();

	currentEntryCursor = 0;

	var selectLesson = document.getElementById('exoLesson');
	var lesson = parseInt(selectLesson.options[selectLesson.selectedIndex].value);
	if(lesson) lessonNumber = lesson;
	
	var checkboxPrevious = document.getElementById('exoPrevious');
	lessonPrevious = (checkboxPrevious.checked ? 1 : 0);
	
	dataLoaded = false; }

function speed() {
	var selectSpeed = document.getElementById('exoSpeed');
	var speed = selectSpeed.options[selectSpeed.selectedIndex].value;
	if(speed) lessonSpeed = speed;
	setTimer();
	if(t1.status==1) { t1.stop(); t2.stop(); t2.start(); t1.start(); } }
	
function setTimer() {
	t1.clock = lessonSpeed * 1000;
	t2.clock = t1.clock * 4; }

/**
 *
 *	Load Data
 *
 */
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

var dataLoaded = false;
function loadData() {
	if(dataLoaded) return false;
	parseContents();
	dataLoaded = true; }

function parseContents() {
	entries = new Array();
	if(!lessonLines) return;
	var nbLines = lessonLines.length;
	if(nbLines==0) return;
	var line = new String();
	var entry;
	var lesson;
	var cpt = 0;
	
	for(var i=0; i<nbLines; i++) {
		line = lessonLines[i];
		line = line.trim();
		entry = line.split("\t");
		lesson = parseInt(entry[0]);
		
		if(lesson > lessonNumber) continue;
		if(lesson < lessonNumber && lessonPrevious == 0) continue;

		entry = entry.splice(1, 3);
		if(entry[2] && entry[2].length>0) {
			entries[cpt] = entry;
			cpt++; } }

	nbEntries = entries.length; }