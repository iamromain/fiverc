/* Tabs Menu */

var menu = new Array();
menu[1] = new Array(['Universities', '/study/universities.html'], ['Language schools', '/study/schools.html'], ['Study in Beijing', '/study/china/beijing'], ['Study in Shanghai', '/study/china/shanghai'], ['Study in Guilin', '/study/china/guangxi/guilin']);
menu[2] = menu[1];
menu[3] = menu[1];
menu[4] = new Array(['Study in China forum', '/forum/list.html?q=22'], ['General forums', '/forum'], ['The China Club', '/club']);
menu[5] = new Array(['International Student Recruitment', '/study/partners/'], ['Partner programs', '/study/partners/programs.html'], ['Advertising programs', '/study/partners/advertising.html'], ['Contact us', '/study/partners/contact.html']);

var menudefault = 1;

function showit(which) {
  thecontent = menuBuildContents(which);
  if(which.length==0) return;
  var menuobj = document.getElementById ? document.getElementById("submenu") : document.all ? document.all.submenu : document.layers? document.dep1.document.dep2 : ""
  if (document.getElementById||document.all) menuobj.innerHTML = thecontent
  else if (document.layers){ menuobj.document.write(thecontent); menuobj.document.close() } }

function menuBuildContents(which) {
  submenus = menu[which];
  if(!submenus || submenus.length<1) return '';
  var str = '<ul>';
  var n = submenus.length - 1;
  for(var i=0; i<submenus.length; i++) { str += '<li'+((i==n ? ' class=\"last\"' : ''))+'><a href="'+submenus[i][1]+'">'+submenus[i][0]+'</a></li>'; }
  str += '</ul>';
  return str; }