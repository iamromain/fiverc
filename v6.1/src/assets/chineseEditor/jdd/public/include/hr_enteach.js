/* Tabs Menu */

var menu = new Array();
menu[1] = new Array();
menu[2] = new Array();
menu[3] = new Array(['Work in China forum', '/forum/list.html?q=21'], ['General forums', '/forum'], ['The China Club', '/club']);

var menudefault = 3;

function showit(which) {
  thecontent = menuBuildContents(which);
  if(which.length==0) return;
  var menuobj = document.getElementById ? document.getElementById("submenu") : document.all ? document.all.submenu : document.layers? document.dep1.document.dep2 : ""
  if (document.getElementById||document.all) menuobj.innerHTML = thecontent
  else if (document.layers){ menuobj.document.write(thecontent); menuobj.document.close() } }

function menuBuildContents(which) {
  submenus = menu[which];
  if(!submenus || submenus.length<1) return '';
  var str = '<ul>';
  var n = submenus.length - 1;
  for(var i=0; i<submenus.length; i++) { str += '<li'+((i==n ? ' class=\"last\"' : ''))+'><a href="'+submenus[i][1]+'">'+submenus[i][0]+'</a></li>'; }
  str += '</ul>';
  return str; }