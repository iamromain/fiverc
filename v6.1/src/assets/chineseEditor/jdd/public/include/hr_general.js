/* Tabs Menu */

var menu = new Array();
menu[1] = new Array( ['People China', '/china/people'], ['Daily horoscope', '/astrology/horoscope']);
menu[2] = new Array(['Mandarin audio courses', '/learn/chinese'], ['Write chinese characters', '/learn/characters'], ['Chinese vocabulary lists', '/chinese/vocabulary']);
menu[3] = new Array(['Cn «» En', '/tools/dictionary.html'], ['Character dictionary', '/tools/sinograms.html'], ['Synonyms thesaurus', '/tools/synonyms.html'], ['Slang', '/chinese/slang'], ['Example', '/chinese/example'], ['Chengyu dictionary', '/chinese/chengyu/dictionary']);
menu[4] = new Array(['Annotation', '/tools/annotation.html'], ['Pronunciation', '/tools/pinyin.html'], ['Chinese editor', '/tools/ime.html'], ['Pinyin editor', '/tools/pinyin-editor.html'], ['Trad. » Simp.', '/tools/converter-tradsimp.html'], ['Simp. » Trad.', '/tools/converter-simptrad.html']);
menu[5] = new Array(['Name translation', '/names'], ['Chinese tattoo', '/tattoo'], ['Name on Calligrapy', '/tools/calligraphy.html'], ['Name on Painting', '/tools/name-painting.html'], ['Name on Seal', '/tools/seal.html']);
menu[6] = new Array(['Chinese painting', '/learn/painting'], ['Chinese calligraphy', '/learn/calligraphy'], ['Chinese Songs', '/songs'], ['Proverbs', '/chinese/proverbs'], ['Astrology', '/astrology'], ['Caricatures', '/caricatures']);
menu[7] = new Array(['China forum', '/forum'], ['China Club (language exchange)', '/club']);

var menudefault = 1;

function showit(which) {
  thecontent = menuBuildContents(which);
  if(which.length==0) return;
  var menuobj = document.getElementById ? document.getElementById("submenu") : document.all ? document.all.submenu : document.layers? document.dep1.document.dep2 : ""
  if (document.getElementById||document.all) menuobj.innerHTML = thecontent
  else if (document.layers){ menuobj.document.write(thecontent); menuobj.document.close() } }

function menuBuildContents(which) {
  submenus = menu[which];
  if(!submenus || submenus.length<1) return '';
  var str = '<ul>';
  var n = submenus.length - 1;
  for(var i=0; i<submenus.length; i++) { str += '<li'+((i==n ? ' class="last"' : ''))+'><a href="'+submenus[i][1]+'">'+submenus[i][0]+'</a></li>'; }
  str += '</ul>';
  return str; }