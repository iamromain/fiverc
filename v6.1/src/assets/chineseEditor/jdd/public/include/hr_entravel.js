/* Tabs Menu */

var menu = new Array();
menu[1] = new Array(['China Travel Blog', '/china/travel'], ['China Travel Forum', '/forum/list.html?q=23'], ['China Club', '/club']);
menu[2] = menu[1];
menu[3] = new Array(['Design your own tour (free quotation)', '/travel/custom.html'], ['Inquiry', '/travel/inquiry.html']);
menu[4] = new Array(['Get Train Tickets Online', '/travel/train.html']);
menu[5] = new Array(['Yangtze cruise (5 days)', '/travel/yangtze/5day-tour.html'], ['Yangtze cruise (8 days)', '/travel/yangtze/8day-tour.html'], ['Yangtze deluxe cruise (12 days)', '/travel/yangtze/12day-tour.html']);
menu[6] = new Array(['Everest Trekking Tour (8 days)', '/travel/tibet/8day-tour.html'], ['Sightseeing Tour (10 days)', '/travel/tibet/10day-tour.html'], ['Sightseeing Tour (15 days)', '/travel/tibet/15day-tour.html']);


var menudefault = 1;

function showit(which) {
  thecontent = menuBuildContents(which);
  if(which.length==0) return;
  var menuobj = document.getElementById ? document.getElementById("submenu") : document.all ? document.all.submenu : document.layers? document.dep1.document.dep2 : ""
  if (document.getElementById||document.all) menuobj.innerHTML = thecontent
  else if (document.layers){ menuobj.document.write(thecontent); menuobj.document.close() } }

function menuBuildContents(which) {
  submenus = menu[which];
  if(!submenus || submenus.length<1) return '';
  var str = '<ul>';
  var n = submenus.length - 1;
  for(var i=0; i<submenus.length; i++) { str += '<li'+((i==n ? ' class="last"' : ''))+'><a href="'+submenus[i][1]+'">'+submenus[i][0]+'</a></li>'; }
  str += '</ul>';
  return str; }

function changebg(divname) {
	var rand = Math.round(Math.random() * 6) + 1;
	var image = '/jdd/public/documents/travel/chinacustom' + rand + '.jpg';
	document.getElementById(divname).style.background = 'url("' + image + '") repeat scroll 0 0 transparent';
}