import firebase from 'firebase'
import firestore from 'firebase/firestore'
// Initialize Firebase
var config = {
    apiKey: "AIzaSyB3vwcss_Qyf0kTrw5xxP4ZIltS0xK-8AU",
    authDomain: "achinejournalv6.firebaseapp.com",
    databaseURL: "https://achinejournalv6.firebaseio.com",
    projectId: "achinejournalv6",
    storageBucket: "achinejournalv6.appspot.com",
    messagingSenderId: "360271005682"
  };
  const firebaseApp = firebase.initializeApp(config);
 firebaseApp.firestore().settings({ timestampsInSnapshots: true })

 export default firebaseApp.firestore()